import lombok.Data;
import lombok.val;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.internals.KafkaTopicPartition;
import org.apache.flink.streaming.util.serialization.KeyedDeserializationSchema;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.io.IOException;
import java.util.*;


@Data
class KafkaMsg {
	private String key;
	private String value;
	private String topic;
	private Integer partition;
	private Long offSet;

	public KafkaMsg() {
	}

	public KafkaMsg(String key, String value, String topic, Integer partition, Long offSet) {
		this.key = key;
		this.value = value;
		this.topic = topic;
		this.partition = partition;
		this.offSet = offSet;
	}
}

class TypedKeyedDeserializationSchema implements KeyedDeserializationSchema<KafkaMsg> {

	@Override
	public KafkaMsg deserialize(byte[] key, byte[] value, String topic, int partition, long offset) throws IOException {
		System.out.println("key = " + key);
		if (key == null){
			key=new byte[0] ;
		}
		System.out.println("value = " + value);
		System.out.println("topic = " + topic);
		System.out.println("partition = " + partition);
		System.out.println("offset = " + offset);
		return new KafkaMsg(new String(key),new String(value),topic,partition,offset);
	}

	@Override
	public boolean isEndOfStream(KafkaMsg kafkaMsg) {
		return false;
	}

	@Override
	public TypeInformation<KafkaMsg> getProducedType() {
		return TypeInformation.of(KafkaMsg.class);
	}
}


public class FlinkKafkaConsumer1 {
	public static void main(String[] args) throws Exception {
		// 0 初始化 flink 环境
		StreamExecutionEnvironment env =
				StreamExecutionEnvironment.getExecutionEnvironment();
		env.setParallelism(1);
		// 1 kafka 消费者配置信息
		Properties properties = new Properties();
		properties.setProperty("group.id","test");
//		properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "hadoop102:9092");
		properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "100.100.100.1:9092");
		// 2 创建 kafka 消费者
		FlinkKafkaConsumer<KafkaMsg> kafkaConsumer = new FlinkKafkaConsumer<>(
				"first1",
				new TypedKeyedDeserializationSchema(),
				properties
		);

		// 获取最大offset
		Map map = latest_off("100.100.100.1:9092","first1",0);
		System.out.println("map = " + map);

//		//  指定offset
//		/**
//		 * Flink从topic中最初的数据开始消费
//		 */
//		kafkaConsumer.setStartFromEarliest();
//		/**
//		 * Flink从topic中指定的时间点开始消费，指定时间点之前的数据忽略
//		 */
//		kafkaConsumer.setStartFromTimestamp(1559801580000l);
		/**
		 * Flink从topic中指定的offset开始，这个比较复杂，需要手动指定offset
		 */
		// 定义offset map
		HashMap<KafkaTopicPartition, Long> kafkaTopicPartitionLongHashMap = new HashMap<>();
		// topic 分区  作为key
		KafkaTopicPartition partition = new KafkaTopicPartition("first1", 0);
		// offset 作为 value
		Long offset = 0L ;
		kafkaTopicPartitionLongHashMap.put(partition, offset);

		kafkaConsumer.setStartFromSpecificOffsets(kafkaTopicPartitionLongHashMap);
//		/**
//		 * Flink从topic中最新的数据开始消费
//		 */
//		kafkaConsumer.setStartFromLatest();
//		/**
//		 * Flink从topic中指定的group上次消费的位置开始消费，所以必须配置group.id参数
//		 */
//		kafkaConsumer.setStartFromGroupOffsets();

		// 3 消费者和 flink 流关联
		env.addSource(kafkaConsumer).print();
		// 4 执行
		env.execute();
	}




	public static Map latest_off(String broker, String topic, Integer partition) {
		Properties props = new Properties() ;
		props.put("bootstrap.servers", broker);
		props.put("group.id", "offsetHunter");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		KafkaConsumer  consumer = new KafkaConsumer(props);
		Collection<TopicPartition> partitions = new ArrayList<>();

		TopicPartition topicPartition = new TopicPartition(topic, partition);
		partitions.add(topicPartition);

		Map map = consumer.endOffsets(partitions);
		return map ;
	}

}



