import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;


// 未完成
public class PostgresSink extends RichSinkFunction {
    Connection conn = null;
    PreparedStatement ps = null;
    String url = "jdbc:postgres://100.100.100.1:5432/mytest?useUnicode=true&characterEncoding=utf-8&useSSL=false";
    String username = "mytest";
    String password = "mytest";


    //
    @Override
    public void open(Configuration parameters) throws Exception {
        // 获取mysql 连接
        conn = DriverManager.getConnection(url, username, password);
        // 关闭自定提交
        conn.setAutoCommit(false);
    }

    // 关闭方法
    @Override
    public void close() throws Exception {
        if (conn != null) {
            conn.close();
        }
        if (ps != null) {
            ps.close();
        }
    }
    @Override
    public void invoke(Object value, Context context) throws Exception {
        super.invoke(value, context);
    }
}
