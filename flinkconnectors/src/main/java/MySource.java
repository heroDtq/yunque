/**
 *
 *
 * @Date: 2023/11/15 23:24
 * @Description:
 **/

import org.apache.flink.streaming.api.functions.source.SourceFunction;

/**
 * 自定义数据源
 */
public  class MySource implements SourceFunction<VehicleAlarm> {
	@Override
	public void run(SourceFunction.SourceContext<VehicleAlarm> ctx) throws Exception {
		while (true) {
			long id = System.currentTimeMillis();
			VehicleAlarm vehicleAlarm = new VehicleAlarm(String.valueOf(id), "川A" + id,
					"绿", System.currentTimeMillis(), "sc");
			ctx.collect(vehicleAlarm);
			Thread.sleep(10000);
		}
	}

	@Override
	public void cancel() {
	}
}
