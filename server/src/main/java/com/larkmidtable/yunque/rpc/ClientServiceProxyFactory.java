package com.larkmidtable.yunque.rpc;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 *
 * @Date: 2023/8/9 23:35
 * @Description:
 **/
public class ClientServiceProxyFactory {
	public static Object getClientService(Class interfaceClazz,String server) {

		return Proxy.newProxyInstance(interfaceClazz.getClassLoader(), new Class[]{interfaceClazz},
				(proxy, method, args) -> getRPCResulet(method,args,server));

	}

	/**
	 * 通过socket远程连接server调用服务端实现类，并返回结果
	 * @param method 代理的方法
	 * @param args 代理方法参数
	 * @return 远程调用返回结果
	 */
	private static Object getRPCResulet(Method method, Object[] args,String server){
		Object back = null;
		String ip = server.split(":")[0];
		Integer port = Integer.parseInt(server.split(":")[1]);
		ObjectOutputStream objOutStream = null;
		Socket client;
		try {
			client = new Socket(ip,port);
			objOutStream = new ObjectOutputStream(client.getOutputStream());
			objOutStream.writeUTF(method.getName());
			objOutStream.writeObject(method.getParameterTypes());
			objOutStream.writeObject(args);

			try (ObjectInputStream objInStream = new ObjectInputStream(client.getInputStream())) {
//				将服务端的报错信息返回,该位置是否应该返回(msg,Exception) 结构,将服务端的报错堆栈信息报送给服务端?
				back =  objInStream.readObject();
//				back ="调用成功!";
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			try {
				if(null != objOutStream)
					objOutStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return  back;
	}

}
