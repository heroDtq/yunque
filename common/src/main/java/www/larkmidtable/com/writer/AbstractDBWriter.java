package www.larkmidtable.com.writer;

import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import www.larkmidtable.com.bean.ConfigBean;
import www.larkmidtable.com.element.Column;
import www.larkmidtable.com.element.Record;
import www.larkmidtable.com.element.Transformer;
import www.larkmidtable.com.exception.YunQueException;
import www.larkmidtable.com.log.LogRecord;
import www.larkmidtable.com.model.DbTaskParams;
import www.larkmidtable.com.model.DbTaskResult;
import www.larkmidtable.com.model.TaskResult;
import www.larkmidtable.com.util.JVMUtil;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import static www.larkmidtable.com.reader.AbstractDBReader.getCountDownCount;

public abstract class AbstractDBWriter extends Writer {

    private static final Logger logger = LoggerFactory.getLogger(AbstractDBWriter.class);

    private ExecutorService executorService;
    private List<AbstractDbWritTask> tasks;

    protected List<CompletableFuture<TaskResult>> taskFutureResults;

    private String writeRecordSql;

    private List<DbTaskResult> taskResults;

    /**
     * 拆分批量默认值，可考虑覆盖配置
     */
    protected static Integer DEFAULT_BATCH_SIZE = 10000;

    @Override
    public void init() {
        logRecord = LogRecord.newInstance();
        logRecord.start(String.format("%s 初始化", this.getClass().getSimpleName()));
        // 插入语句
        ConfigBean configBean = getConfigBean();
        String table = configBean.getTable();
        String column = configBean.getColumn();
        StringBuilder placeholder = new StringBuilder("?");
        for (int i = 0; i < column.split(",").length - 1; i++) {
            placeholder.append(", ?");
        }
        writeRecordSql = String.format("insert into %s (%s) values (%s)", table, column, placeholder);
        // 线程池
        Integer threadNum = configBean.getThread();
        executorService = Executors.newFixedThreadPool(threadNum, new ThreadFactory() {
            private final AtomicInteger threadNumber = new AtomicInteger(1);

            @Override
            public Thread newThread(@NonNull Runnable r) {
                return new Thread(r, "writer-pool-" + threadNumber.getAndIncrement());
            }
        });
        tasks = new ArrayList<>(threadNum);
        tasks.addAll(getDBWriteTasks());
        logRecord.end();
    }

    public abstract List<AbstractDbWritTask> getDBWriteTasks();

    @Override
    public void write(Queue<Record> queue) {
        logRecord.start(String.format("%s 写入数据", this.getClass().getSimpleName()));
        this.queue = queue;
        taskFutureResults = new ArrayList<>();
        for (AbstractDbWritTask task : tasks) {
            CompletableFuture<TaskResult> completableFuture = CompletableFuture.supplyAsync(task, executorService);
            taskFutureResults.add(completableFuture);
        }
    }

    @Override
    public void stop() {
        JVMUtil.shutdownThreadPool(executorService);
        logRecord.totalStatistics(taskFutureResults);
    }

    public abstract class AbstractDbWritTask extends WriteTask<DbTaskParams, DbTaskResult> {

        protected Connection connection;
        protected DbTaskResult dbTaskResult;

        public AbstractDbWritTask(DbTaskParams taskParams) {
            super(taskParams);
        }

        @Override
        public void preProcess() {
            ConfigBean configBean = this.taskParams.getConfigBean();
            try {
                this.connection = DriverManager.getConnection(configBean.getUrl(), configBean.getUsername(), configBean.getPassword());
                this.connection.setAutoCommit(false);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            dbTaskResult = new DbTaskResult();
        }

        @Override
        public DbTaskResult doProcess() {
            defaultStartWrite();
            return dbTaskResult;
        }

        @Override
        public DbTaskResult postProcess(DbTaskResult processResult) {
            try {
                this.connection.commit();
                this.connection.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            return processResult;
        }

        /**
         * 默认的线程读写处理逻辑，数据库可选择覆写逻辑，实现自己的读写
         */
        public void defaultStartWrite() {
            List<Record> writeBuffer = new ArrayList<>(DEFAULT_BATCH_SIZE);
            Record record;
            int count = 0;
            long startTime = System.currentTimeMillis();
            try {
                long lastTime = startTime;
                while ((record = queue.poll()) != null || getCountDownCount() > 0) {
                    if (record != null) {
                        writeBuffer.add(record);
                        count++;
                    }
                    long currentTime = System.currentTimeMillis();
                    if ((writeBuffer.size() >= DEFAULT_BATCH_SIZE || currentTime - lastTime > 500) && !writeBuffer.isEmpty()) {
                        doBatchInsert(writeBuffer);
                        writeBuffer.clear();
                    }
                    lastTime = currentTime;
                }
                if (!writeBuffer.isEmpty()) {
                    doBatchInsert(writeBuffer);
                    writeBuffer.clear();
                }
                dbTaskResult.ok();
                dbTaskResult.setSuccessRowNum(count);
            } catch (Exception e) {
                dbTaskResult.bad(writeBuffer.size());
                dbTaskResult.setSuccessRowNum(count - writeBuffer.size());
                e.printStackTrace();
            }
            dbTaskResult.setDuration(System.currentTimeMillis() - startTime);
        }

        protected void doBatchInsert(List<Record> buffer) {
            LogRecord logRecord = LogRecord.newInstance();
            logRecord.start("批量插入数据");
            try {
                PreparedStatement preparedStatement = connection.prepareStatement(writeRecordSql);
                for (Record record : buffer) {
                    preparedStatement = fillPreparedStatement(preparedStatement, record);
                    preparedStatement.addBatch();
                }
                preparedStatement.executeBatch();
                connection.commit();
                logRecord.end();
            } catch (Exception e) {
                logRecord.fail();
                throw new YunQueException(
                        Thread.currentThread().getName() + " 插入数据失败" +
                                "\n失败数据条数：" + buffer.size() +
                                "\n第一条失败数据：" + buffer.get(0),
                        e);
            }
        }

        protected PreparedStatement fillPreparedStatement(PreparedStatement preparedStatement, Record record) throws SQLException {
            for (int i = 0; i < record.getColumnNumber(); i++) {
                Column column = Transformer.reverseBuildColumn(record.getColumn(i));
                preparedStatement.setObject(i + 1, column.getRawData());
            }
            return preparedStatement;
        }
    }
}
