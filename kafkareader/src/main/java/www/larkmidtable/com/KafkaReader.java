package www.larkmidtable.com;
import www.larkmidtable.com.element.Record;
import www.larkmidtable.com.reader.Reader;

import java.util.List;
import java.util.Queue;

/**
 * reade kafka topic data
 */
public class KafkaReader extends Reader{

    @Override
    public void open() {

    }

    @Override
    public void init(int pos ,int total) {
        // TODO
    }

    @Override
    public void read(Queue<Record> queue) {
        // TODO
    }

    @Override
    public Queue<List<String>> startRead(String[] inputSplits) {
        return null;
    }

    @Override
    public Queue<List<String>> startRead(String inputSplit) {
        return null;
    }

    @Override
    public String[] createInputSplits(int count,int bcount) {
        return new String[0];
    }

    @Override
    public void close() {

    }

    @Override
    public void stop() {
        // TODO
    }
}
