package www.larkmidtable.com;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import www.larkmidtable.com.concurrent.TaskParams;

import java.sql.Connection;

/**
 * @Author Gooch
 * @Description TODO
 * @Date 2022/11/24 0024 17:11
 */
@Getter
@Setter
@AllArgsConstructor
public class DB2ReaderParams extends TaskParams {
    private Connection connection;
    private String splitSql;
}
